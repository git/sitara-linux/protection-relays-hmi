/*
 * Copyright (C) 2017 Texas Instruments Incorporated - http://www.ti.com/
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#include "protection_relays_hmi.h"
#include "ui_protection_relays_hmi.h"

protection_relays_hmi::protection_relays_hmi(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::protection_relays_hmi)
{
    ui->setupUi(this);
    ui -> stackedWidget -> setCurrentIndex(0);
}

protection_relays_hmi::~protection_relays_hmi()
{
    delete ui;
}

void protection_relays_hmi::on_bayScreens_clicked()
{
     ui -> stackedWidget -> setCurrentIndex(1);
}

void protection_relays_hmi::on_meter_clicked()
{
    ui -> stackedWidget -> setCurrentIndex(1);
}


void protection_relays_hmi::on_monitor_clicked()
{
     ui -> stackedWidget -> setCurrentIndex(1);
}


void protection_relays_hmi::on_reports_clicked()
{
     ui -> stackedWidget -> setCurrentIndex(1);
}


void protection_relays_hmi::on_control_clicked()
{
     ui -> stackedWidget -> setCurrentIndex(1);
}

void protection_relays_hmi::on_device_clicked()
{
     ui -> stackedWidget -> setCurrentIndex(1);
}


void protection_relays_hmi::on_accessLevel_clicked()
{
    ui -> stackedWidget -> setCurrentIndex(1);
}

void protection_relays_hmi::on_settings_clicked()
{
     ui -> stackedWidget -> setCurrentIndex(1);
}

void protection_relays_hmi::on_back_clicked()
{
    ui -> stackedWidget -> setCurrentIndex(0);
}



void protection_relays_hmi::on_exit_clicked()
{
    qApp -> quit();

}
