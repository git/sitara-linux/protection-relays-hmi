#-------------------------------------------------
#
# Project created by QtCreator 2017-08-17T14:31:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = protection_relays_hmi
TEMPLATE = app


SOURCES += main.cpp\
        protection_relays_hmi.cpp

HEADERS  += protection_relays_hmi.h

FORMS    += protection_relays_hmi.ui

RESOURCES += \
    icons.qrc

target.path += /home/root
INSTALLS += target
